#!/bin/bash

# script fails if any individual command fails
set -e

IOC_DIRECTORY=/iocs/mtca01-ifc01-rf-ts2/TS2-010RFC_SC-IOC-214

source "/epics/base-7.0.8/require/5.1.0/bin/activate"
IOCSH="${E3_REQUIRE_BIN}"/iocsh
source "${IOC_DIRECTORY}"/ioc_activate.sh

# activate core dumps
ulimit -S -c 204800  # blocks (100 MB)

"${IOCSH}" \
    -c "var dbThreadRealtimeLock 0" \
    "${IOC_DIRECTORY}"/st.cmd




