# TS2 RFLPS - System 01 - Fast Interlock Module Calibration Tables

| **PV Prefix**                          | **Description**          | **Filename**       |
| -------------------------------------- | ------------------------ | -------------------|
| TS2-010RFC:RFS02:DirC-110:DAPwrFwdDA   | Driver Amplifier Pwr Fwd | calib_data_ch0.csv |
| TS2-010RFC:RFS02:DirC-110:DAPwrFwdPA   | Driver Amplifier Pwr Fwd | calib_data_ch1.csv |
| TS2-010RFC:RFS02:DirC-110:DAPwrFwdL2   | Load 2 Power Forward     | calib_data_ch2.csv |
| TS2-010RFC:RFS02:DirC-110:DAPwrRflctPA | PwrAmp Power Reflect     | calib_data_ch3.csv |
| TS2-010RFC:RFS02:DirC-110:DAPwrFwdC1   | Cavity 1 Pwr. Forward    | calib_data_ch4.csv |
| TS2-010RFC:RFS02:DirC-110:DAPwrFwdC2   | Cavity 2 Pwr. Forward    | calib_data_ch5.csv |
| TS2-010RFC:RFS02:DirC-110:DAPwrFwdL1   | Load 1 Pwr. Forward      | calib_data_ch6.csv |
| TS2-010RFC:RFS02:DirC-110:DAPwrFwdL2   | Load 2 Pwr. Forward      | calib_data_ch7.csv |
| TS2-010RFC:RFS02:DirC-110:DAPwrRflctC1 | Cavity 1 Power Reflect   | calib_data_ch8.csv |
| TS2-010RFC:RFS02:DirC-110:DAPwrRflctC2 | Cavity 2 Power Reflect   | calib_data_ch9.csv |
| TS2-010RFC:RFS02:Mod-110:Cur           | Modulator Current        | calib_data_ch10.csv |
| TS2-010RFC:RFS02:Mod-110:Vol 			 | Modulator Voltage        | calib_data_ch11.csv |
| TS2-010RFC:RFS02:SolPS-110:Cur 		 | Solenoid 1 Current 		| calib_data_ch12.csv |
| TS2-010RFC:RFS02:SolPS-120:Cur 		 | Solenoid 2 Current 		| calib_data_ch13.csv |
| TS2-010RFC:RFS02:SolPS-130:Cur 		 | Solenoid 4 Current 		| calib_data_ch14.csv |
