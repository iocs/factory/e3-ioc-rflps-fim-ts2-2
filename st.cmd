##################################################################
# Startup template for RFLPS Fast Interlock Module IOC
#
# TS2 system 02 - last modified in 2022-08-15
#
##################################################################

###############################################################################
# Required modules and versions
###############################################################################
require asyn
require autosave
require iocstats
require recsync
require iocmetadata

### Main module of the IOC
require nds3epics
require fimioc,2.3.0+1
#
###############################################################################
# Environmental variables settings
###############################################################################
#
### EPICS configurations
epicsEnvSet(EPICS_CA_MAX_ARRAY_BYTES, 10000000)
epicsEnvSet(NELM, 8000)
callbackSetQueueSize(10000)
#
### External file with individual macros for all input channels
iocshLoad("fim_config.iocsh")
#
### SEC-SUB prefix
epicsEnvSet(PREFIX,"TS2-RFLPS2")
epicsEnvSet(PREFIX2,"TS2-010RFC")
epicsEnvSet(IOCDEVICE, "SC-IOC-214")
epicsEnvSet(IOCNAME, "$(PREFIX2)_$(IOCDEVICE)")
#
### Calibration Files Folder
epicsEnvSet("CALIB_FOLDER", "$(E3_CMD_TOP)/calibration")
#
### Timestamp source PV
epicsEnvSet("TSELPV", "TS2-010:Ctrl-EVR-102:20Cnt-I.TIME CA")

###############################################################################
# IOC Access Security File
###############################################################################
asSetFilename("$(E3_CMD_TOP)/fim-access.acf")
#
###############################################################################
# IFC1410 EPICS driver for RFLPS FIM firmware
###############################################################################
ndsCreateDevice(ifc1410_fim, $(PREFIX), card=0, fmc=1, files=$(CALIB_FOLDER=/tmp), winsize=$(NELM), dataformat=0)
#nds setLogLevelDebug $(PREFIX)-FSM
#nds setLogLevelDebug $(PREFIX)-FSM-DI-CH0-ILCK
#
###############################################################################
# Analog Inputs Records
###############################################################################
dbLoadRecords(fimioc.template, "P=$(PREFIX):, R=$(FIMDEVICE):, NDSROOT=$(PREFIX), NDS0=FSM, NELM=$(NELM)")
#
###############################################################################
# Analog Inputs Records
###############################################################################
iocshLoad("$(fimioc_DIR)/fimLoad_AI.iocsh", "IOCDEVICE=$(FIMDEVICE)")
#
###############################################################################
# Digital Inputs Records
###############################################################################
iocshLoad("$(fimioc_DIR)/fimLoad_DI.iocsh", "IOCDEVICE=$(FIMDEVICE)")
#
###############################################################################
# Reflected Power Special Blocks Records
###############################################################################
iocshLoad("$(fimioc_DIR)/fimLoad_special.iocsh", "IOCDEVICE=$(FIMDEVICE)")
#
###############################################################################
# Soft Interlock
###############################################################################
epicsEnvSet("INPUT_CHANNEL", "$(PREFIX):RFS-Mod-110:Vol-ROI-Val")
dbLoadRecords(fimioc-softilck.template, "PREFIX=$(PREFIX), DEVICE=$(FIMDEVICE), P=$(PREFIX):, R=$(FIMDEVICE):, INPUT_CHANNEL=$(INPUT_CHANNEL)")
#
###############################################################################
# autosave Module setup
###############################################################################
epicsEnvSet("AS_REMOTE", "/opt/nonvolatile")
epicsEnvSet("AS_FOLDER", "$(IOCNAME)")

set_savefile_path    ("$(AS_REMOTE)/$(AS_FOLDER)", "/save")
set_requestfile_path ("$(AS_REMOTE)/$(AS_FOLDER)", "/req")
#
system("install -d $(AS_REMOTE)/$(AS_FOLDER)/save")
system("install -d $(AS_REMOTE)/$(AS_FOLDER)/req")
#
save_restoreSet_status_prefix("$(PREFIX2):$(IOCDEVICE):AS-")
save_restoreSet_Debug(0)
save_restoreSet_IncompleteSetsOk(1)
save_restoreSet_DatedBackupFiles(1)
save_restoreSet_NumSeqFiles(3)
save_restoreSet_SeqPeriodInSeconds(300)
save_restoreSet_CAReconnect(1)
save_restoreSet_CallbackTimeout(-1)
#
dbLoadRecords("save_restoreStatus.db", "P=$(PREFIX2):$(IOCDEVICE):AS-, DEAD_SECONDS=5")
#
afterInit("makeAutosaveFileFromDbInfo('$(AS_REMOTE)/$(AS_FOLDER)/req/values_pass1.req','autosaveFields_pass1')")
afterInit("create_monitor_set('values_pass1.req','5')")
afterInit("fdbrestore("$(AS_REMOTE)/$(AS_FOLDER)/save/values_pass1.sav")")
#

###############################################################################
# iocStats Module setup
###############################################################################
epicsEnvSet("IOCSTATSPREFIX", "$(PREFIX2):$(IOCDEVICE)")
iocshLoad("$(iocstats_DIR)/iocStats.iocsh", "IOCNAME=$(IOCSTATSPREFIX)")
#
###############################################################################
# recSync Module setup
###############################################################################
epicsEnvSet("RECSYNCPREFIX", "$(PREFIX2):$(IOCDEVICE)")
iocshLoad("$(recsync_DIR)/recsync.iocsh", "IOCNAME=$(RECSYNCPREFIX)")
#
###############################################################################
# Sequencer setup
###############################################################################
epicsEnvSet("SEQ_1", "$(PREFIX):$(DEV_DI0):$(CHN_DI0)-isFirstIlck")
epicsEnvSet("SEQ_2", "$(PREFIX):$(DEV_DI0):$(CHN_DI0)-IlckRst")
epicsEnvSet("SEQ_3", "$(PREFIX):$(DEV_DI1):$(CHN_DI1)-isFirstIlck")
epicsEnvSet("SEQ_4", "$(PREFIX):$(DEV_DI1):$(CHN_DI1)-IlckRst")
afterInit('seq fimioc_automation_seq "P=$(PREFIX):, R=$(FIMDEVICE):, HVON_FST=$(SEQ_1), HVON_RST=$(SEQ_2), RFON_FST=$(SEQ_3), RFON_RST=$(SEQ_4)"')
#
###############################################################################
# Create metadata for save-restore and archiver
###############################################################################
#
pvlistFromInfo("ARCHIVE_THIS", "$(PREFIX2):$(IOCDEVICE):ArchiverList")
pvlistFromInfo("SAVRES_THIS", "$(PREFIX2):$(IOCDEVICE):SavResList")
#
###############################################################################
# Launch IOC engine
###############################################################################
iocInit()
